// Import server startup through a single index entry point

import './fixtures.js';
import './register-api.js';

Accounts.emailTemplates.siteName = "Test";
Accounts.emailTemplates.from = "Test <admin@test.com>";

Accounts.emailTemplates.verifyEmail = {
  subject() {
    return "[Test] Verify Your Email Address";
  },
  text(user, url) {
    let emailAddress = user.emails[0].address,
      urlWithoutHash = url.replace('#/', ''),
      supportEmail = "spiralworks@test.com",
      emailBody = `To verify your email address (${emailAddress}) visit the following link:\n\n${urlWithoutHash}\n\n If you did not request this verification, please ignore this email. If you feel something is wrong, please contact our support team: ${supportEmail}.`;

    return emailBody;
  }
};

Meteor.startup(() => {
  process.env.MAIL_URL = "smtps://richieroldantest:chiechie19@smtp.gmail.com:465"
 
})

// SMS.phoneTemplates = {   from: '+9729999999',   text: function (user, code) {
//       return 'Welcome your invitation code is: ' + code;   } }; SMS.send =
// function (options) {   console.log("SMS.send",options) };