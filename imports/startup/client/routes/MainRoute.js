import {FlowRouter} from 'meteor/kadira:flow-router';
import {mount, withOptions} from 'react-mounter';
import React, {Component} from 'react';

/* Layout */
import LoginLayout from '../../../ui/layouts/LoginLayout';
import MainLayout from '../../../ui/layouts/MainLayout';

/* Pages */
import LoginPageContainer from '../../../ui/pages/LoginPage';
import HomePageContainer from '../../../ui/pages/HomePage';
import SignupPageContainer from '../../../ui/pages/SignupPage';
import SignupEmailPageContainer from '../../../ui/pages/SignupEmailPage';
import SignupPhonePageContainer from '../../../ui/pages/SignupPhonePage';

/* Components */
import HeaderComponentContainer from '../../../ui/components/HeaderComponent';
import FooterComponentContainer from '../../../ui/components/FooterComponent';

let mainRoute = FlowRouter.group({

    triggersEnter: [function (context, redirect) {
            //Trigger enter
        }
    ]
});

mainRoute.route('/login', {
    action() {
        mount(MainLayout, {page: <LoginPageContainer/>})
    },
    triggersEnter: [function (context, redirect) {
            if (Meteor.userId()) {
                redirect('/')
            }
        }
    ]

});

mainRoute.route('/signup', {
    action() {
        mount(MainLayout, {page: <SignupPageContainer/>})
    },
    triggersEnter: [function (context, redirect) {
            if (Meteor.userId()) {
                redirect('/')
            }
        }
    ]

});

mainRoute.route('/signup/email', {
    action() {
        mount(MainLayout, {page: <SignupEmailPageContainer/>})
    },
    triggersEnter: [function (context, redirect) {
            if (Meteor.userId()) {
                redirect('/')
            }
        }
    ]

});

mainRoute.route('/signup/phone', {
    action() {
        mount(MainLayout, {page: <SignupPhonePageContainer/>})
    },
    triggersEnter: [function (context, redirect) {
            if (Meteor.userId()) {
                redirect('/')
            }
        }
    ]

});

mainRoute.route('/verify-email/:token', {
    action(params) {
        Accounts.verifyEmail(params.token, (error) => {
            if (error) {
                alert(error.reason);
            } else {
                FlowRouter.go('/');
                alert('Email verified! Thanks!');
            }
        });
    }
});

mainRoute.route('/', {

    action() {

        mount(MainLayout, {
            header: <HeaderComponentContainer/>,
            page: <HomePageContainer/>
        })
    },
    triggersEnter: [function (context, redirect) {
            if (!Meteor.userId()) {
                redirect('/login')
            }
        }
    ]
});
