import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { mount, withOptions } from 'react-mounter';

import 'bootstrap/dist/css/bootstrap.css';

import MainLayout from '../../ui/layouts/MainLayout';
// import NotFoundPage from '../../ui/pages/misc/NotFoundPage.jsx';

import './routes/MainRoute';



FlowRouter.wait();
Tracker.autorun(function() {
    if (Roles.subscription.ready() && !FlowRouter._initialized) {
        FlowRouter.initialize()
    }
})
Meteor.startup(() => {
  render(<div></div>, document.getElementById('app'));
});


FlowRouter.notFound = {
  action() {
    mount(MainLayout,{ main: <NotFoundPage /> })
  },
};
