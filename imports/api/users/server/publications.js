// All user-related publications

import { Meteor } from 'meteor/meteor';


Meteor.publish(null, function () {
  
    if (this.userId) {
        return Meteor
          .users
          .find({
            _id: this.userId
          }, {
            fields: {
              'phone': 1
            }
          });
      } else {
        this.ready();
      }
});
