import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/meteor';
import {HTTP} from 'meteor/meteor';
import {signupWithPhone, sendVerificationLink, sendPhoneVerificationCode,verifyPhone} from './methods'

describe('Users Methods', () => {

    test('Signup via phone', async() => {

        signupWithPhone({
            username: "test",
            phone: {
                number: "09366787635",
                verified: false
            }
        })
        expect(Meteor.users.update).toBeCalled()
        expect(Meteor.call).toBeCalled()

    })

    test('Send Verificatin Link', async() => {

        sendVerificationLink()
        expect(Accounts.sendVerificationEmail).toBeCalled()

    })

    test('Send Phone Verification Code', async() => {

        sendPhoneVerificationCode({
            username: "test",
            phone: {
                number: "09366787635",
                verified: false
            }
        })
        expect(Meteor.users.update).toBeCalled()
        expect(HTTP.call).toBeCalled()

    })


    test('Verify User', async() => {

        verifyPhone("testID","testCode")
        expect(Meteor.users.update).toBeCalled()
    

    })
})