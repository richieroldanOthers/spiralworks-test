// Methods related to links

import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';
import {Accounts} from 'meteor/accounts-base'
import {HTTP} from 'meteor/http'
// import { Links } from './links.js';

const signupWithPhone = async(user) => {
  let u = Meteor
    .users
    .update({
      username: user.username
    }, {
      $set: {
        phone: user.phone
      }
    })
  // Send Code
  Meteor.call("sendPhoneVerificationCode", user)
}

const sendVerificationLink = async() => {
  let userId = Meteor.userId();
  if (userId) {
    return Accounts.sendVerificationEmail(userId);
  }
}

const sendPhoneVerificationCode = async(user) => {

  let code = Math
    .random()
    .toString(36)
    .substr(2, 4)
    .toUpperCase()
  Meteor
    .users
    .update({
      username: user.username
    }, {
      $set: {
        phoneVerificationCode: code
      }
    })

  let phone = user.phone.number
  let message = `Welcome! your verification code is: ${code}`

  HTTP.call('POST', 'https://www.itexmo.com/php_api/api.php', {
    params: {
      "1": phone,
      "2": message,
      "3": "TR-RICHI760735_IIDG9"
    }
  }, (error, result) => {
    if (!error) {
      console.log(result)
    } else {
      console.log("SENDING SMS ERROR :", error)
    }
  });

  // console.log(message,phone)
}

const verifyPhone = async(userId, code) => {
  let user = Meteor
    .users
    .findOne(userId)

  if (user) {
    if (user.phoneVerificationCode == code) {
      Meteor
        .users
        .update(userId, {
          $set: {
            "phone.verified": true
          }
        })
    } else {
      throw new Meteor.Error('verifyphone', 'Invalid Code');
    }
  }
}



Meteor.methods({sendVerificationLink, signupWithPhone, sendPhoneVerificationCode, verifyPhone})
export {signupWithPhone, sendVerificationLink, sendPhoneVerificationCode, verifyPhone}