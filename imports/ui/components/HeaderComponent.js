import React, {Component} from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {
    Toggle,
    AppBar,
    IconButton,
    MenuItem,
    FlatButton,
    IconMenu,
    Drawer
} from 'material-ui';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import {FlowRouter} from 'meteor/kadira:flow-router';

export class HeaderComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false
        }

        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout() {
        Meteor.logout(()=>{
            this.setState({open: false})
            FlowRouter.go('/login')
        })
       
    }
    render() {
        return (
            <div>
                <AppBar title="Spiralworks"
                onLeftIconButtonClick={()=>{
                    this.setState({open:true})
                }}
                />

                <Drawer
                    docked={false}
                    width={200}
                    open={this.state.open}
                    onRequestChange={(open) => this.setState({open})}>
                    <MenuItem onClick={this.handleLogout}>Log Out</MenuItem>
                    
                </Drawer>
            </div>
        )
    }
}

export default HeaderComponentContainer = withTracker(() => {

    return {};
})(HeaderComponent);
