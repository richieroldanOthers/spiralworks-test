import React, {Component} from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {FlowRouter} from 'meteor/kadira:flow-router';

import {Paper, TextField, RaisedButton, FlatButton, FloatingActionButton} from 'material-ui';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';

export class VerifyPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password : ''
        }
    }


    _submit(event) {

        event.preventDefault();
     

        let user = {
            email: this.state.email,
            password: this.state.password
        };

        Accounts.createUser(user, (error) => {
            if (error) {
                alert(error.reason);
            } else {
                Meteor.call('sendVerificationLink', (error, response) => {
                    if (error) {
                        alert(error.reason);
                    } else {
                        alert('Welcome!');
                    }
                });
            }
        });
    }

    render() {
        return (
            <div className={'container'}>
                <div className={'row justify-content-sm-center'}>
                    <div className={'col-md-8 col-lg-5'}>
                        <Paper
                            zDepth={2}
                            style={{
                            marginTop: '3em',
                            padding: 40
                        }}>

                           Verify
                        </Paper>
                    </div>

                </div>
            </div>
        )
    }
}

export default VerifyPageContainer = withTracker(() => {

    return {};
})(VerifyPage);
