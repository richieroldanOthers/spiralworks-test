import React, {Component} from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {FlowRouter} from 'meteor/kadira:flow-router';

import {Paper, TextField, RaisedButton, FlatButton, FloatingActionButton} from 'material-ui';

export class HomePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      code: ''
    }
  }



  _resendVerificationCode(){
    this.setState({
      loading: true
    })
    Meteor.call('sendPhoneVerificationCode',Meteor.user(),(error, response) => {
      if(error){
        alert("Something went wrong")
      }else{
        
      }
      this.setState({
        loading: false
      })
    })
  }

  _submit(){
    this.setState({
      loading: true
    })
    Meteor.call('verifyPhone',Meteor.user(),this.state.code,(error, response) => {
      if(error){
        alert(error.reason)
      }else{
        alert('Account Verified')
      }
      this.setState({
        loading: false
      })
    })
  }
  

  render() {
    let user = Meteor.user()
    if(!user){
      return  null
    }
    let isVerified = false
    let phone = false

    if(user.phone){
      phone = true
      isVerified = user.phone.verified
    }else{
      phone = false
      isVerified = user.emails[ 0 ].verified
    }
   
    return (
      <div className={'container'}>
        <div className={'row'}>

          <div className={'col'}>

            {!isVerified ? phone ? 
             <Paper
             zDepth={2}
             style={{
             marginTop: '3em',
             padding: 15,
             color: 'rgba(201, 0, 0, 0.87)',
         
           }}>
            
            <h5 style={{
              marginBottom:0
            }}>To view your profile please Verify your account. <br/>
            <TextField  hintText={'Enter code'} type={'text'} name={'code'}   onChange={(e)=>{
                  this.setState({
                      code:e.target.value
                  })
              }} />
              <RaisedButton
                  label={this.state.loading ? "Please wait..." : "Submit"}
                
                  primary
                  style={{
                    marginLeft: 10
                }}
                  disabled={this.state.loading}
                 
                  onClick={this._submit.bind(this)}
              />
              <RaisedButton
                  label={this.state.loading ? "Please wait..." : "Resend Verification Code"}
                
                  secondary
                  style={{
                    marginLeft: 10
                }}
                  disabled={this.state.loading}
                 
                  onClick={this._resendVerificationCode.bind(this)}
              />



            </h5>
            </Paper>

            : 
            

            <Paper
            zDepth={2}
            style={{
            marginTop: '3em',
            padding: 10,
            color: 'rgba(201, 0, 0, 0.87)',
            backgroundColor: 'rgb(240, 234, 126)'
          }}>
            <h5 style={{
              marginBottom:0
            }}>To view your profile please Verify your account. <a href="#" onClick={()=>{

              if(!this.state.loading){
                this.setState({
                  loading: true
                })

                
                  Meteor.call( 'sendVerificationLink', ( error, response ) => {
                    if ( error ) {
                      alert( error.reason);
                    } else {
                      let email = user.emails[ 0 ].address;
                      alert( `Verification sent to ${ email }!`);
                    }
                    this.setState({
                      loading: false
                    })
                  });
                }

               
            
             
            }}>Resend Link</a> {this.state.loading ? <i>sending link...</i> : null}</h5>
          </Paper>

            : null
            
            }


            <Paper
              zDepth={2}
              style={{
              marginTop: '1em',
              padding: 40
            }}>

              <h2>Welcome {user.profile
                  .firstName}!</h2><br/><br/>
              <h4><b>Here's your profile:</b></h4><br/>

              <h3>
                First Name :
                
                <b>{ isVerified ? user.profile
                  .firstName : <h4><i>Verify Account to see content</i></h4> } </b></h3>
              <h3>
               Last Name :
               
                <b>{ isVerified ? user.profile
                  .lastName : <h4><i>Verify Account to see content</i></h4> } </b></h3>
              <h3>
               
                {phone ? "Phone:" : " Email Address:"}
                  
              </h3>

              { isVerified ? phone ? <h3 ><b>{user.phone.number}  </b></h3> : user.emails.map((email, index) => {
                  return <h3 key={index}><b>{email.address}  </b></h3>
                }) : <h4><i>Verify Account to see content</i></h4> }
            </Paper>
          </div>
        </div>

      </div>
    )
  }
}

export default HomePageContainer = withTracker(() => {
  Meteor.subscribe('users');
  let user = Meteor.user()
  return {user};
})(HomePage);
