import React, {Component} from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {FlowRouter} from 'meteor/kadira:flow-router';

import {Paper, TextField, RaisedButton, FlatButton, FloatingActionButton} from 'material-ui';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';

export class SignupPage extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'container'}>
                <div className={'row justify-content-sm-center'}>
                    <div className={'col-md-8 col-lg-5'}>
                        <Paper
                            zDepth={2}
                            style={{
                            marginTop: '3em',
                            padding: 40
                        }}>

                            <FloatingActionButton
                                style={{
                                top: '35px',
                                left: '0px',
                                position: 'absolute'
                            }}
                            onClick={()=>{
                                FlowRouter.go('/login')
                            }}
                            >
                                <ArrowBack/>
                            </FloatingActionButton>

                            <h3>Signup with:</h3>

                            <RaisedButton
                                label="Email"
                                primary
                                labelStyle={{
                                fontSize: 20
                            }}
                                fullWidth
                                style={{
                                marginTop: 20
                            }}
                                onClick={()=>{
                                    FlowRouter.go('/signup/email')
                                }}
                            
                            />
                            <RaisedButton
                                label="Phone"
                                fullWidth
                                labelStyle={{
                                fontSize: 20
                            }}
                                style={{
                                marginTop: 20
                            }}
                                onClick={() => {

                                    FlowRouter.go('/signup/phone')
                                }}/>

                            {/* <RaisedButton
                                label="Facebook"
                                fullWidth
                                labelStyle={{
                                fontSize: 20,
                                color: 'white'
                            }}
                                style={{
                                marginTop: 20
                            }}
                                buttonStyle={{
                                backgroundColor: '#3b5998'
                            }}
                                onClick={() => {}}/> */}

                        </Paper>
                    </div>

                </div>
            </div>
        )
    }
}

export default SignupPageContainer = withTracker(() => {

    return {};
})(SignupPage);
