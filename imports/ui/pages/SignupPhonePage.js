import React, {Component} from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {FlowRouter} from 'meteor/kadira:flow-router';

import {Paper, TextField, RaisedButton, FlatButton, FloatingActionButton} from 'material-ui';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';

export class SignupPhonePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            phone : '',
            password : '',
            firstName : '',
            lastName : '',
            loading:false
        }
    }


    _submit(event) {

        event.preventDefault();
     
        this.setState({
            loading: true
        })
       
        let user = {
            username : this.state.phone, 
            phone : {
                number : this.state.phone,
                verified: false
            },
            password :this.state.password,
            profile: {
              firstName: this.state.firstName,
              lastName: this.state.lastName
            }
          }

          
          Accounts.createUser(user,(e)=>{
            if(e){
                alert("User already exist")
                this.setState({
                    loading: false
                })
            }else{
                Meteor.call('signupWithPhone',user,(error, response) => {
                    this.setState({
                        loading: false
                    })
                    if(error){
                        console.log(error)
                        alert("Something went wrong")
                    }else{
                        FlowRouter.go('/')
                    }
                
                })
            }
          })
       
    }

    render() {
        return (
            <div className={'container'}>
                <div className={'row justify-content-sm-center'}>
                    <div className={'col-md-8 col-lg-5'}>
                        <Paper
                            zDepth={2}
                            style={{
                            marginTop: '3em',
                            padding: 40
                        }}>

                            <FloatingActionButton
                                style={{
                                top: '35px',
                                left: '0px',
                                position: 'absolute'
                            }}
                            onClick={()=>{
                                FlowRouter.go('/signup')
                            }}
                            >
                                <ArrowBack/>
                            </FloatingActionButton>

                            <h3>Signup with Email</h3>
                            <form action="" onSubmit={this._submit.bind(this)}>
                           

                            <TextField hintText={'Phone ex: 09067867388'} title={'Input must be 11 digit number'} type={'text'} pattern="[0-9]{11}" name={'phone'} fullWidth onChange={(e)=>{
                                this.setState({
                                    phone:e.target.value
                                })
                            }} />
                            <TextField hintText={'Password'} type={'password'} name={'password'}  fullWidth onChange={(e)=>{
                                this.setState({
                                    password:e.target.value
                                })
                            }} />


                            <TextField hintText={'First Name'} type={'text'} name={'firstName'} fullWidth onChange={(e)=>{
                                this.setState({
                                    firstName:e.target.value
                                })
                            }} />
                            <TextField hintText={'Last Name'} type={'text'} name={'lastName'}  fullWidth onChange={(e)=>{
                                this.setState({
                                    lastName:e.target.value
                                })
                            }} />
                            
                            

                            <RaisedButton
                                label={this.state.loading ? "Please wait..." : "Signup"}
                                fullWidth
                                primary
                                type={'submit'}
                                labelStyle={{fontSize: 20,}}
                                disabled={this.state.loading}
                                style={{
                                marginTop: 20
                            }}
                                onClick={this._submit.bind(this)}
                            />

                                </form>

                        </Paper>
                    </div>

                </div>
            </div>
        )
    }
}

export default SignupPhonePageContainer = withTracker(() => {

    return {};
})(SignupPhonePage);
