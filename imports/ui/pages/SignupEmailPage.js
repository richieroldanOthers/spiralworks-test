import React, {Component} from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {FlowRouter} from 'meteor/kadira:flow-router';

import {Paper, TextField, RaisedButton, FlatButton, FloatingActionButton} from 'material-ui';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';

export class SignupEmailPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password : '',
            loading:false
        }
    }


    _submit(event) {

        event.preventDefault();
     
        this.setState({
            loading: true
        })
        let user = {
            username: this.state.email,
            email: this.state.email,
            password: this.state.password,
            profile: {
                firstName : this.state.firstName,
                lastName: this.state.lastName
            }
        };

        Accounts.createUser(user, (error) => {
            if (error) {
                alert(error.reason);
                this.setState({
                    loading: false
                })
            } else {
                Meteor.call('sendVerificationLink', (error, response) => {
                    if (error) {
                        alert(error.reason);
                    } else {
                        alert('Registration Success!, Please wait for your verification email');
                        FlowRouter.go('/')
                    }
                    this.setState({
                        loading: false
                    })
                });
            }
        });
    }

    render() {
        return (
            <div className={'container'}>
                <div className={'row justify-content-sm-center'}>
                    <div className={'col-md-8 col-lg-5'}>
                        <Paper
                            zDepth={2}
                            style={{
                            marginTop: '3em',
                            padding: 40
                        }}>

                            <FloatingActionButton
                                style={{
                                top: '35px',
                                left: '0px',
                                position: 'absolute'
                            }}
                            onClick={()=>{
                                FlowRouter.go('/signup')
                            }}
                            >
                                <ArrowBack/>
                            </FloatingActionButton>

                            <h3>Signup with Email</h3>
                            <form action="" onSubmit={this._submit.bind(this)}>
                           

                            <TextField hintText={'Email'} type={'email'} name={'email'} fullWidth onChange={(e)=>{
                                this.setState({
                                    email:e.target.value
                                })
                            }} />
                            <TextField hintText={'Password'} type={'password'} name={'password'}  fullWidth onChange={(e)=>{
                                this.setState({
                                    password:e.target.value
                                })
                            }} />


                            <TextField hintText={'First Name'} type={'text'} name={'firstName'} fullWidth onChange={(e)=>{
                                this.setState({
                                    firstName:e.target.value
                                })
                            }} />
                            <TextField hintText={'Last Name'} type={'text'} name={'lastName'}  fullWidth onChange={(e)=>{
                                this.setState({
                                    lastName:e.target.value
                                })
                            }} />
                            
                            

                            <RaisedButton
                                label={this.state.loading ? "Please wait..." : "Signup"}
                                fullWidth
                                primary
                                labelStyle={{fontSize: 20,}}
                                disabled={this.state.loading}
                                type={'submit'}
                                style={{
                                marginTop: 20
                            }}
                                onClick={this._submit.bind(this)}
                            />

                                </form>

                        </Paper>
                    </div>

                </div>
            </div>
        )
    }
}

export default SignupEmailPageContainer = withTracker(() => {

    return {};
})(SignupEmailPage);
