import React, {Component} from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {FlowRouter} from 'meteor/kadira:flow-router';

import {Paper, TextField, RaisedButton, FlatButton} from 'material-ui';

export class LoginPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            loading: false
        }

        this.handleLogin = this
            .handleLogin
            .bind(this)
    }

  

    handleLogin() {
        this.setState({loading: true})
        

        Meteor.loginWithPassword(this.state.username, this.state.password, (e) => {
            if (e) {
                alert(e.reason)
            } else {
                FlowRouter.go('/')
            }
            this.setState({loading: false})
        })

    }

    render() {
        return (
            <div className={'container'}>
                <div className={'row justify-content-sm-center'}>
                    <div className={'col-md-8 col-lg-5'}>
                        <Paper
                            zDepth={2}
                            style={{
                            marginTop: '3em',
                            padding: 30
                        }}>

                            <h3>Welcome to Spiralworks!</h3>

                            <form onSubmit={this.handleLogin}>

                            <div className={'clearfix'}>
                                <TextField
                                    hintText={'Email / Phone'}
                                    onChange={(e) => {
                                    this.setState({username: e.target.value})
                                }}
                                    fullWidth/>
                                <TextField
                                    type={'password'}
                                    hintText={'Password'}
                                    fullWidth
                                    style={{
                                    marginTop: 10
                                }}
                                    onChange={(e) => {
                                    this.setState({password: e.target.value})
                                }}/>

                                <RaisedButton
                                    label={this.state.loading
                                    ? "Please wait..."
                                    : "Login"}
                                    primary
                                    disabled={this.state.loading}
                                    fullWidth
                                    type={'submit'}
                                    style={{
                                    marginTop: 15
                                }}
                                    onClick={this.handleLogin}/>
                                <RaisedButton
                                    label="Register"
                                    fullWidth
                                    style={{
                                    marginTop: 15
                                }}
                                    onClick={() => {
                                    FlowRouter.go('/signup')
                                }}/>

                            </div>
                            </form>

                        </Paper>
                    </div>

                </div>
            </div>
        )
    }
}

export default LoginPageContainer = withTracker(() => {

    return {};
})(LoginPage);
