import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import '../stylesheets/main'

export default class MainLayout extends Component {

    constructor(props) {
        super(props)
    }
    render() {
        return (
            <MuiThemeProvider>
               <div>
                {this.props.header}
                {this.props.page}
                {this.props.footer}
               </div>
            </MuiThemeProvider>
        )
    }
}
