let userSetupResult = [];
export function usersQueryResult(result) {
    userSetupResult = result;
}
export const Meteor = {
  users: {
    findOne: jest.fn().mockImplementation(() => usersQueryResult),
    find: jest.fn().mockImplementation(() => ({
      fetch: jest.fn().mockReturnValue(usersQueryResult),
      count: jest.fn(),
    })),
    update: jest.fn()
  },
  call : jest.fn().mockImplementation((name)=>({
    
  })),
  methods : jest.fn(),
  userId : jest.fn().mockReturnValueOnce('testID')
};
export const Mongo = {
  Collection: jest.fn().mockImplementation(() => ({
    _ensureIndex: (jest.fn()),
  })),
};

export const Accounts = {
  sendVerificationEmail : jest.fn()
}

export const HTTP = {
  call : jest.fn()
}