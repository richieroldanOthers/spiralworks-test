// Client entry point, imports all client code

import '/imports/startup/client';
import '/imports/startup/both';


if(Meteor.isProduction){
    if (location.protocol != 'https:')
    {
     location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
    }
}
