# Spiralworks Test

### Requirements : 

Build an App with following Modules:
- Backend
    - Use PHP/NodeJS/Python/GoLang
    - Boilerplate App (Register, Login, Profile)
    - Register over email / phone number
- Frontend
    - Use for the frontend VueJS/React/Angular
    - it should be PWA standard
    - should reach lighthouse score of 80
    - Mobile friendly
- Deployment
    - Build an automation to deploy and run the App
    - Document everything inside of the README.md

## Live Demo
https://spiralworks-test.herokuapp.com/

### Getting Started

- Install NodeJS (https://nodejs.org/en/download/)
- Install Meteor (https://www.meteor.com/install)
- Clone the Project
    ```
    $ git clone https://gitlab.com/richieroldanOthers/spiralworks-test.git
    ```
- Install Dependencies
    ```
    $ cd spiralworks-test
    $ meteor npm install
    $ meteor
    ```
- Run the app
     ```
    $ meteor
    ```

### Project Structure
- Followed Meteor Project Structure (https://guide.meteor.com/structure.html)

    | Path | Remark |
    | ------ | ------ |
    | /.meteor | Meteor Files |
    | /client | Codes that execute on client |
    | /server | Codes that execute on client |
    | /public | Place for public files (images,fonts) |
    | /private | Place for private files (keys, certs) |
    | /imports | All codes codes are in here |
    | /imports/api | Models/Methods/Publish (https://docs.meteor.com/api/pubsub.html) |
    | /imports/startup | Startup codes |
    | /imports/ui | All UI related code |

    
### Framework/Libraries
- Meteor Framework
- ReactJS
- Bootstrap
- Material UI

### Services Used

- Gmail - Used IMAP for sending email 
    - Account used : richieroldantest@gmail.com
- iTexmo - Used for sending SMS (https://itexmo.com/)
    - **NOTE: I only have free subscription on this service so I can only have 10 sms per day**
- Mlab - MongoDB database
    - Included in heroku
    - uri : (mongodb://heroku_26ll3zmk:3mdmqdvhj5epila8pijhukge3t@ds251548.mlab.com:51548/heroku_26ll3zmk)

### Deployment
- I used Heroku to deploy the app (https://www.heroku.com/). 
- I am the one who has the account therefore I'll be the one who can publish it.
- Auto deployment is already configured
- Auto deployment is triggered when a user ***git push*** to heroku git repo in branch master.
- Heroku git repo : https://git.heroku.com/spiralworks-test.git

### PWA / Lighthouse
- Light house PWA rating is : **91**

    ![alt text](https://gitlab.com/richieroldanOthers/spiralworks-test/raw/master/light%20house%20rating.png "Light house Rating")



**If you have questions and clarification Please contact me. Thank you!**

## &copy; Richie Roldan